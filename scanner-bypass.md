# Scan through cloudflare, without bypass

```bash
atumscan -u veracross.com -silent -c 200
```

```
[azure-domain-tenant] [http] [info] https://login.microsoftonline.com:443/veracross.com/v2.0/.well-known/openid-configuration [9c6c2aef-2592-46ae-9199-cc8567ac6e74]
[tech-detect:cloudflare] [http] [info] https://veracross.com
[ssl-dns-names] [ssl] [info] veracross.com:443 [veracross.com]
[ssl-issuer] [ssl] [info] veracross.com:443 [Cloudflare, Inc.]
[wordpress-wordpress-importer:detected_version] [http] [info] https://veracross.com/wp-content/plugins/wordpress-importer/readme.txt [0.8.1] [last_version="0.8.1"]
[txt-fingerprint] [dns] [info] veracross.com ["MS=ms65112391","apple-domain-verification=jxmgIDMnDdEd0soA","docusign=7a86ad7a-f623-4bf8-9403-032464f62994","hppgnnadnb0d87leqi0rheb2tm","on1j1k4pcj8ro5mag9daebfl5l","pardot939573=4385e7fc5737dcf7a2260bf833c644b9d3067c7c9c8cc9c1589b0b38254771a3","q40q6i3enuvuande94e5app6b","7ha7ts0fmm1gqeoakfmhnb7aj2","v=spf1 include:us._netblocks.mimecast.com include:usb._netblocks.mimecast.com include:spf.protection.outlook.com include:_spf.salesforce.com include:aspmx.pardot.com include:_spf.intacct.com ~all","asv=78f80c9ec2d3c8450d5f57653ea98e89","mgverify=3d8a5a48da77e0df61753144dfce90a91595f48001fb27080ad4f7062fb59fea","slack-domain-verification=GiV4Yv5J00lLjlfwQhOotcGZ004qxprEupchw0uh","_globalsign-domain-verification=y0D047LiL_wrb8H73Rcx2KJ7N-V9jbPMlC2Upim6tm","atlassian-domain-verification=wLKCDHtjRJOtxdoM9OGl6NkMPKFakwhg5thinjyvmoUI/VGamsTfwkFw0Bx52IPZ","csgl89uf64ldhpputvblg2ug2v","globalsign-domain-verification=R5q_5sku7sL0HCHi2_v4MMJJhzATZGzKuJkKolg-zc","intacct-esk=8F892F690EA7334CE053AA06A8C0F8B3","atlassian-domain-verification=T9ySOWkmRwRSpodHibK3t7xod8TxUvKLnwHXvfkirQkGR3wadJNEv7rZca7yTIdr","MS=ms56404774"]
[http-missing-security-headers:referrer-policy] [http] [info] https://veracross.com
[http-missing-security-headers:clear-site-data] [http] [info] https://veracross.com
[http-missing-security-headers:cross-origin-embedder-policy] [http] [info] https://veracross.com
[http-missing-security-headers:cross-origin-opener-policy] [http] [info] https://veracross.com
[http-missing-security-headers:permissions-policy] [http] [info] https://veracross.com
[http-missing-security-headers:x-permitted-cross-domain-policies] [http] [info] https://veracross.com
[http-missing-security-headers:x-frame-options] [http] [info] https://veracross.com
[http-missing-security-headers:x-content-type-options] [http] [info] https://veracross.com
[http-missing-security-headers:cross-origin-resource-policy] [http] [info] https://veracross.com
[http-missing-security-headers:strict-transport-security] [http] [info] https://veracross.com
[http-missing-security-headers:content-security-policy] [http] [info] https://veracross.com
[dmarc-detect] [dns] [info] _dmarc.veracross.com ["v=DMARC1; p=none; pct=100; rua=mailto:24a21ed7799a984@rep.dmarcanalyzer.com,mailto:re+zxmxmobodyb@dmarc.postmarkapp.com; ruf=mailto:24a21ed7799a984@for.dmarcanalyzer.com; aspf=r; fo=1;"]
[nameserver-fingerprint] [dns] [info] veracross.com [ns-1450.awsdns-53.org.,ns-1582.awsdns-05.co.uk.,ns-51.awsdns-06.com.,ns-636.awsdns-15.net.]
[wordpress-tablepress:detected_version] [http] [info] https://veracross.com/wp-content/plugins/tablepress/readme.txt [2.1.4] [last_version="2.1.3"]
[caa-fingerprint] [dns] [info] veracross.com [godaddy.com,letsencrypt.org,amazon.com]
[wordpress-post-types-order:detected_version] [http] [info] https://veracross.com/wp-content/plugins/post-types-order/readme.txt [2.0.9] [last_version="2.0.5"]
[wordpress-wordfence:detected_version] [http] [info] https://veracross.com/wp-content/plugins/wordfence/readme.txt [7.9.3] [last_version="7.9.3"]
[wordpress-wp-file-manager:detected_version] [http] [info] https://veracross.com/wp-content/plugins/wp-file-manager/readme.txt [7.1.9] [last_version="7.1.9"]
[mx-fingerprint] [dns] [info] veracross.com [10 usb-smtp-inbound-1.mimecast.com.,10 usb-smtp-inbound-2.mimecast.com.]
[wordpress-imagify:detected_version] [http] [info] https://veracross.com/wp-content/plugins/imagify/readme.txt [2.1.1] [last_version="2.1.1"]
[wordpress-adminimize:detected_version] [http] [info] https://veracross.com/wp-content/plugins/adminimize/readme.txt [1.11.9] [last_version="1.11.9"]
[wordpress-header-footer:detected_version] [http] [info] https://veracross.com/wp-content/plugins/header-footer/readme.txt [3.2.5] [last_version="3.2.5"]
[wordpress-classic-editor:detected_version] [http] [info] https://veracross.com/wp-content/plugins/classic-editor/readme.txt [1.6.3] [last_version="1.6.3"]
[wordpress-regenerate-thumbnails:detected_version] [http] [info] https://veracross.com/wp-content/plugins/regenerate-thumbnails/readme.txt [3.1.5] [last_version="3.1.5"]
[wordpress-redirection:detected_version] [http] [info] https://veracross.com/wp-content/plugins/redirection/readme.txt [5.3.10] [last_version="5.3.10"]
[tls-version] [ssl] [info] veracross.com:443 [tls12]
[tls-version] [ssl] [info] veracross.com:443 [tls13]
[wordpress-detect:version_by_css] [http] [info] https://veracross.com/wp-admin/install.php [6.2.2]
[wordpress-wordpress-seo:detected_version] [http] [info] https://veracross.com/wp-content/plugins/wordpress-seo/readme.txt [20.8] [last_version="20.8"]
[wordpress-ewww-image-optimizer:detected_version] [http] [info] https://veracross.com/wp-content/plugins/ewww-image-optimizer/readme.txt [7.0.2] [last_version="7.0.1"]
[wordpress-worker:detected_version] [http] [info] https://veracross.com/wp-content/plugins/worker/readme.txt [trunk] [last_version="trunk"]
[wordpress-popup-maker:detected_version] [http] [info] https://veracross.com/wp-content/plugins/popup-maker/readme.txt [1.18.1] [last_version="1.18.1"]
[waf-detect:cloudflare] [http] [info] https://veracross.com/
[waf-detect:nginxgeneric] [http] [info] https://veracross.com/
[wordpress-post-smtp:detected_version] [http] [info] https://veracross.com/wp-content/plugins/post-smtp/readme.txt [2.5.6] [last_version="2.5.5"]
[wordpress-user-role-editor:detected_version] [http] [info] https://veracross.com/wp-content/plugins/user-role-editor/readme.txt [4.63.3] [last_version="4.63.3"]
[wordpress-cloudflare:detected_version] [http] [info] https://veracross.com/wp-content/plugins/cloudflare/readme.txt [4.12.0] [last_version="4.12.0"]
[robots-txt-endpoint] [http] [info] https://veracross.com/robots.txt
[wordpress-the-events-calendar:detected_version] [http] [info] https://veracross.com/wp-content/plugins/the-events-calendar/readme.txt [6.0.13.1] [last_version="6.0.13.1"]
```

# Scan with bypass

The bypass mode uses a special technique we developed called **InfraDNA**, this is how we can lightly touch and connect to only the systems we need or want to. We can still use a single host. Infact, this scan was done from a host that is _blocked by Cloudflare_.

```bash
atumscan -u veracross.com -silent -c 200  -bypass
```
```
[ssl-dns-names] [ssl] [info] veracross.com:443 [www.veracross.com,veracross.com]
[ssl-issuer] [ssl] [info] veracross.com:443 [Lets Encrypt]
[txt-fingerprint] [dns] [info] veracross.com ["csgl89uf64ldhpputvblg2ug2v","slack-domain-verification=GiV4Yv5J00lLjlfwQhOotcGZ004qxprEupchw0uh","hppgnnadnb0d87leqi0rheb2tm","mgverify=3d8a5a48da77e0df61753144dfce90a91595f48001fb27080ad4f7062fb59fea","q40q6i3enuvuande94e5app6b","on1j1k4pcj8ro5mag9daebfl5l","pardot939573=4385e7fc5737dcf7a2260bf833c644b9d3067c7c9c8cc9c1589b0b38254771a3","MS=ms56404774","MS=ms65112391","_globalsign-domain-verification=y0D047LiL_wrb8H73Rcx2KJ7N-V9jbPMlC2Upim6tm","asv=78f80c9ec2d3c8450d5f57653ea98e89","atlassian-domain-verification=T9ySOWkmRwRSpodHibK3t7xod8TxUvKLnwHXvfkirQkGR3wadJNEv7rZca7yTIdr","intacct-esk=8F892F690EA7334CE053AA06A8C0F8B3","v=spf1 include:us._netblocks.mimecast.com include:usb._netblocks.mimecast.com include:spf.protection.outlook.com include:_spf.salesforce.com include:aspmx.pardot.com include:_spf.intacct.com ~all","7ha7ts0fmm1gqeoakfmhnb7aj2","apple-domain-verification=jxmgIDMnDdEd0soA","atlassian-domain-verification=wLKCDHtjRJOtxdoM9OGl6NkMPKFakwhg5thinjyvmoUI/VGamsTfwkFw0Bx52IPZ","docusign=7a86ad7a-f623-4bf8-9403-032464f62994","globalsign-domain-verification=R5q_5sku7sL0HCHi2_v4MMJJhzATZGzKuJkKolg-zc"]
[tech-detect:nginx] [http] [info] https://veracross.com
[xss-deprecated-header] [http] [info] https://veracross.com [1; mode=block]
[wordpress-readme-file] [http] [info] https://veracross.com/wp/readme.html
[mx-fingerprint] [dns] [info] veracross.com [10 usb-smtp-inbound-2.mimecast.com.,10 usb-smtp-inbound-1.mimecast.com.]
[dmarc-detect] [dns] [info] _dmarc.veracross.com ["v=DMARC1; p=none; pct=100; rua=mailto:24a21ed7799a984@rep.dmarcanalyzer.com,mailto:re+zxmxmobodyb@dmarc.postmarkapp.com; ruf=mailto:24a21ed7799a984@for dmarcanalyzer.com; aspf=r; fo=1;"]
[robots-txt-endpoint] [http] [info] https://veracross.com/robots.txt
[tls-version] [ssl] [info] veracross.com:443 [tls12]
[tls-version] [ssl] [info] veracross.com:443 [tls13]
[openssh-detect] [tcp] [info] veracross.com:22 [SSH-2.0-OpenSSH_7.6p1 Ubuntu-4ubuntu0.5]
[caa-fingerprint] [dns] [info] veracross.com [amazon.com,godaddy.com,letsencrypt.org]
[nameserver-fingerprint] [dns] [info] veracross.com [ns-1450.awsdns-53.org.,ns-1582.awsdns-05.co.uk.,ns-51.awsdns-06.com.,ns-636.awsdns-15.net.]
[azure-domain-tenant] [http] [info] https://login.microsoftonline.com:443/veracross.com/v2.0/.well-known/openid-configuration [9c6c2aef-2592-46ae-9199-cc8567ac6e74]
[unrestricted-remote-forwarding] [ssh] [medium] veracross.com:22 [OpenSSH_7.6p1_Ubuntu]
```

# manual validation
```bash
ssh -v veracross.com
```

```
OpenSSH_8.9p1 Ubuntu-3ubuntu0.1, OpenSSL 3.0.2 15 Mar 2022
debug1: Connecting to veracross.com [52.202.81.112] port 22.
debug1: Connection established.
debug1: Local version string SSH-2.0-OpenSSH_8.9p1 Ubuntu-3ubuntu0.1

```

# Finding Discovered with Bypass Mode

## Medium - Unrestricted Remote Forwarding Despite PermitRemoteOpen Restriction

Dynamic reverse forwarding does not restrict hosts as expected when using the PermitRemoteOpen option. The issue allows connections to hosts that should be restricted by the configuration, potentially leading to unauthorized network access.

- Classification: Security Misconfiguration / Improper Restriction of Network Communication
- CVSS v3 Vector: CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:C/C:L/I:L/A:N
- Base Score: 6.3
- Severity: Medium

## Advanced Vulnerbility Scanning
Not all security bugs receive Common Vulnerabilities and Exposures (CVE) identifiers for various reasons. One common reason is that not all vulnerabilities are disclosed publicly. In some cases, a bug may be silently patched by the vendor or developer without public acknowledgment or disclosure. These silent patches are often included in software updates or change logs.

Tracking change logs is one way to discover vulnerabilities that may have been silently patched. A change log is a record of the changes made to a software application or system. It typically includes information about bug fixes, new features, and security updates. By examining change logs, security researchers can analyze the modifications made to the software and identify potential vulnerabilities that were addressed.

Discovering vulnerabilities through change logs requires expertise in software analysis and security research. Researchers examine the details provided in the change log entries to understand the nature of the fixes or updates. They may look for patterns, specific keywords, or changes that indicate security-related modifications. This process requires careful analysis and often involves reverse engineering or code review techniques to fully understand the impact of the changes.

## Discovered by Atumcell Labs
The "SSH Unrestricted Remote Forwarding Despite PermitRemoteOpen Restriction" vulnerability was tracked by Atumcell Labs via SSH change-logs and a template was added to the vulnerability scanner.
